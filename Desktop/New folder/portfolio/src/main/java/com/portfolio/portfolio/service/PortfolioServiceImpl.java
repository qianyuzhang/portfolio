package com.portfolio.portfolio.service;

import java.util.Collection;
import java.util.Optional;

import com.portfolio.portfolio.entities.Portfolio;
import com.portfolio.portfolio.repo.PortfolioRepo;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortfolioServiceImpl implements PortfolioService{
    private Collection<Portfolio> portfolioList;

    @Autowired
    private PortfolioRepo repo;

    @Override
    public void addPortfolio(Portfolio portfolio) {
        repo.insert(portfolio);
    }

    @Override
    public void updateCash(ObjectId portfolioId, double cash) {
        Optional<Portfolio> p = repo.findById(portfolioId);
        Portfolio p1 = p.get();
        //double originCash = p1.getCash();
        System.out.print(p1.getCash());
        System.out.print(p1.getCash());
        p1.setCash(p1.getCash()+cash);
        repo.save(p1);
    }

    @Override
    public Portfolio getPortfoliobyId(ObjectId portfolioId) {
        Optional<Portfolio> p = repo.findById(portfolioId);
        return p.get();
    }

    @Override
    public void removePortfolio(ObjectId portfolioId) {
        if (repo.findById(portfolioId).isPresent()){
            repo.deleteById(portfolioId);
        }
    }

    @Override
    public Collection<Portfolio> getAllPortfolio(){
        return repo.findAll();
    }

}
