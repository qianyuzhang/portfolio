package com.portfolio.portfolio.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.portfolio.portfolio.controller.IDToString;
import com.portfolio.portfolio.controller.StringToID;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Portfolio {
    
    @Id
    @JsonSerialize(converter = IDToString.class)
    @JsonDeserialize(converter = StringToID.class)
    private ObjectId id;
    private double cash;

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

}
