package com.portfolio.portfolio;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.ArgumentMatchers.isNull;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.portfolio.portfolio.controller.PortfolioController;
import com.portfolio.portfolio.entities.Portfolio;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import org.bson.types.ObjectId;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=PortfolioApplication.class)

public class PortfolioControllerIntegrationTest {
    public static final String TEST_ID = "5f63e0606f44ab0d682e4552";
    

    @Autowired
    private PortfolioController controller;

    @Test
    public void testAddPortfolio(){
        Portfolio Portfolio = new Portfolio(100);
        controller.addPortfolio(Portfolio);
        Iterable<Portfolio> Portfolios = controller.getAllPortfolio();
        Stream<Portfolio> stream = StreamSupport.stream(Portfolios.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }

    @Test
    public void testGetALLPortfolio() {
        Iterable<Portfolio> Portfolios = controller.getAllPortfolio();
        Stream<Portfolio> stream = StreamSupport.stream(Portfolios.spliterator(), false);
        assertThat(stream.count(), equalTo(1L)); 
    }

   @Test
    public void testGetPortfolioById() {
        Portfolio Portfolio = controller.getPortfoliobyId(TEST_ID);
        assertNotNull(Portfolio);
    } 


    @Test
    public void testUpdatePortfolioStatus() {
        //controller.updatePortfolioStatus(TEST_ID, 200);
        Portfolio Portfolio = controller.getPortfoliobyId(TEST_ID);
        
        assertThat(Portfolio.getCash(), equalTo(300.0));
    }
    
}
