package com.portfolio.portfolio.service;

import java.util.Collection;

import com.portfolio.portfolio.entities.Portfolio;

import org.bson.types.ObjectId;

public interface PortfolioService {
    void addPortfolio(Portfolio portfolio);
    void updateCash(ObjectId portfolioId, double cash);
    Portfolio getPortfoliobyId(ObjectId portfolioId);
    Collection<Portfolio> getAllPortfolio();
    void removePortfolio(ObjectId portfolioId);
}
