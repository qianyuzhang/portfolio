package com.portfolio.portfolio.repo;

import java.util.Collection;
import java.util.List;

import com.portfolio.portfolio.entities.Portfolio;

import org.bson.types.ObjectId;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface PortfolioRepo extends MongoRepository<Portfolio, ObjectId> {
    // public List<Portfolio> findByTicker(String ticker);
}
