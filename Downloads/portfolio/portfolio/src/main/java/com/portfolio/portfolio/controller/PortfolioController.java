package com.portfolio.portfolio.controller;

import java.util.Collection;

import com.portfolio.portfolio.entities.Portfolio;
import com.portfolio.portfolio.service.PortfolioService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/portfolio")
public class PortfolioController {
    @Autowired
    private PortfolioService pService;

    @RequestMapping(method=RequestMethod.POST, value = "/{id}")
    public void addPortfolio(@RequestBody Portfolio portfolio){
        pService.addPortfolio(portfolio);
    }

    @RequestMapping(value="/portfolio/id/{id}", method=RequestMethod.GET)
    public Portfolio getPortfoliobyId(@PathVariable("id") String portfolioId) {
        return pService.getPortfoliobyId(new ObjectId("" + portfolioId));
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public Collection<Portfolio> getAllPortfolio() {
        return pService.getAllPortfolio();
    }

    @RequestMapping(method=RequestMethod.DELETE, value = "/{id}")
    public void deletePortfoliobyId(@PathVariable("id") String PortfolioID) throws HttpClientErrorException{
        pService.removePortfolio(new ObjectId(""+PortfolioID));
    }

    @RequestMapping(method=RequestMethod.PATCH, value = "/updateCash/{id}" )
    public void updatePortfolioStatus(@PathVariable("id") String PortfolioId, @RequestBody double cash) throws HttpClientErrorException{
        pService.updateCash(new ObjectId("" + PortfolioId), cash);
    }

}
